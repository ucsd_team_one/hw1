#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String
from geometry_msgs.msg import Twist, Pose, TwistWithCovariance, PoseWithCovariance, Transform, TransformStamped
from nav_msgs.msg import Odometry
import tf
x = 0.0
y = 0.0
dummy = 0.0
the_pose = Pose()
the_pose.position.x = 1.0
the_pose.position.y = 1.0
the_pose.position.z = 1.0
the_pose.orientation.x = 0.0
the_pose.orientation.y = 0.0
the_pose.orientation.z = 0.0
the_pose.orientation.w = 0.0

zero_tf = Transform()
zero_tf.translation.x = 0.0
zero_tf.translation.y = 0.0
zero_tf.translation.z = 0.0
zero_tf.rotation.x = 0.0
zero_tf.rotation.y = 0.0
zero_tf.rotation.z = 0.0
zero_tf.rotation.w = 0.0


def callback(data):
    global x
    global y
    global the_pose
    global dummy
    global zero_tf
    the_pose.position.x += data.linear.x
    the_pose.position.y += data.linear.y
    the_pose.position.z += data.linear.z
    (x2, y2, z2, w2) = tf.transformations.quaternion_from_euler(
        data.angular.x, data.angular.y, data.angular.z)
    the_pose.orientation.x += x2
    the_pose.orientation.y += y2
    the_pose.orientation.z += z2
    the_pose.orientation.w += w2
    br = tf.TransformBroadcaster()
    br.sendTransform((0.0, 0.0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     rospy.Time.now(),
                     "",
                     "zero_tf")
    br.sendTransform((1.0, 1.0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     rospy.Time.now(),
                     "zero_tf",
                     "base_footprint")
    br.sendTransform((the_pose.position.x, the_pose.position.y, the_pose.position.z),
                     tf.transformations.quaternion_from_euler(
                         the_pose.orientation.x, the_pose.orientation.y, the_pose.orientation.z),
                     rospy.Time.now(),
                     "zero_tf",
                     "odom")

    dummy = dummy + 1
    msg = Odometry()
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = '/odom'  # i.e. '/odom'
    msg.child_frame_id = 'base_footprint'  # i.e. '/base_footprint'

    cov = np.array([0.0] * 36).reshape(6, 6)
    msg.pose.covariance = tuple(cov.ravel().tolist())

    msg.twist.covariance = tuple(cov.ravel().tolist())

    msg.pose.pose = the_pose
    msg.twist.twist = data

    pub2 = rospy.Publisher('/location', String, queue_size=10)
    pub = rospy.Publisher('/odom', Odometry, queue_size=10)
    pub.publish(msg)
    rospy.loginfo("Just sent a message")
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    # if data.data == 'w':
    #     x = x+  1
    # if data.data == 'a':
    #     y = y - 1
    # if data.data == 's':
    #     x = x - 1
    # if data.data == 'd':
    #     y = y + 1
    # pub2.publish("x: " + str(x) + " y: " + str(y))


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    # rospy.Subscriber("direction_cmds", String, callback)
    rospy.Subscriber("cmd_vel", Twist, callback)
    # pub = rospy.Publisher("location", String, queue_size=10)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
