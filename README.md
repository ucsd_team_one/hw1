# HW1 Set up and send position

## 1 Targets
* Install Ubuntu and ROS on laptop and odroid
* Make controller node running on laptop and receiver node running on odroid
* Calculate updated position on odroid

## 2 Steps
### 2.1 Set up workspace
Assume our workspace is called `robot`.
```sh
$ mkdir -p ~/robot/src
$ cd ~/robot/src
$ catkin_init_workspace
$ cd ~/robot/
$ catkin_make
```

### 2.2 Clone and make hw1
```sh
$ cd ~/robot/src
$ git clone https://yourname@bitbucket.org/ucsdteam1/hw1.git # replace yourname
$ cd ~/robot
$ catkin_make
$ source ~/robot/devel/setup.bash # tell ros our directory
```

### 2.3 Running
Currently, need to source `setup.bash` for each terminal.

Terminal 1: roscore
```sh
$ source ~/robot/devel/setup.bash
$ roscore
```
Termianl 2: controller
```sh
$ source ~/robot/devel/setup.bash
$ rosrun hw1 create_controller.py # tab complition shall work here
```
Termianl 3: receiver
```sh
$ source ~/robot/devel/setup.bash
$ rosrun hw1 cmd_receiver.py
```
Terminal 4: rviz
```sh
$ source ~/robot/devel/setup.bash
$ rosrun rviz rviz
```
